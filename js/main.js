const $ourServicesButton = $('.ourServicesButton');
const $tabs = $('.ourServicesData div[data-tab]');

$ourServicesButton.on('click', function() {
    const serviceTabIndex = $(this).index();
    $ourServicesButton.removeClass('ourSerActiveBtn');
    $(this).addClass('ourSerActiveBtn');
    $tabs.each(function(index) {
        if (index === serviceTabIndex) {
            $tabs.eq(index).css({ 'display': 'flex' });
        } else {
            $tabs.eq(index).css({ 'display': 'none' });
        }
    })
});

$(document).ready(function() {
    $(".slider").slick({
        arrows: true,
        slidesToShow: 2,
        centerMode: true,
        asNavFor: ".sliderBig",
        draggable: false
    });
    $('.sliderBig').slick({
        arrows: false,
        fade: true,
        asNavFor: ".slider",
        draggable: false
    })
});

buttons.onclick = function(e) {
    if (e.target.tagName == "BUTTON")
        filter.className = e.target.getAttribute('data-cat')
}